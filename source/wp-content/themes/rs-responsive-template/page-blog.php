<?php get_header(); ?> 
    <?php query_posts(array( 'paged' => get_query_var('paged') )); ?>
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>    
        <!-- do stuff ... -->
        <div class="primary-row">            
            <h2><span><?php the_title(); ?></span></h2>            
            <div class="post-row-content">                
                <div class="post-row-meta">
                    <i class="fa fa-clock-o"></i><?php the_time('l, F jS, Y'); ?>
                    <i class="fa fa-tags"></i><?php the_category(' , ', get_the_id()); ?>
                    <i class="fa fa-user"></i><span style="color:red;"><?php the_author_link(); ?></span>
                </div>
                <div class="post-row-description"><?php the_content(); ?></div>                    
            </div><!-- end post-row-content -->
        </div><!-- end primary-row -->
        <?php endwhile; ?>    
        <div class="primary-row">
            <?php if(function_exists('wp_pagenavi')) wp_pagenavi(); ?>
        </div>
    <?php endif; ?>
    <?php wp_reset_query(); ?>     
<?php get_footer(); ?>