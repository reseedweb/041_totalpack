<?php get_template_part('header'); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->	
	<div class="row clearfix">
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<p class="mb20"><img src="<?php bloginfo('template_url'); ?>/img/content/company_content_img.jpg" alt="company" /></p>	
			<h3 class="h3-title">企業理念</h3>
			<div class="company-text">
				<p>昭和商会のダンボール製品は、お客様の商品をより良く素早くその用途に応じ確実に「PACK」し、輸送や保管を通して信頼と満足を提供いたします。<br />
				   また常に変化する経営環境に対し、スピーディに柔軟かつ適正に対応し、自らの可能性に挑戦します。<br />
				   会社及び社員の独自能力の育成と向上を目指し、健康で安全な工場環境を築き、一人ひとりが充実感を持って働ける会社を目指します。<br />
				   また地域環境に配慮した廃棄物の適正処理、リサイクルの推進に努め、社会への貢献とお客様の発展に寄与できることを喜びとし、社業の繁栄と社員の幸福を目指します。
				</p>
			</div>
		</div>
	</div>	
</div><!-- end primary-row -->
<div class="primary-row clearfix"><!-- begin primary-row -->	
		<div class="row clearfix">
			<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h3 class="h3-title">会社概要</h3>				
				<table class="company-table-content">
					<tbody>
						<tr>
							<th>社名</th>
							<td>株式会社 昭和商会(ショウワショウカイ)</td>
						</tr>
						<tr>
							<th>所在地</th>
							<td>〒615-0015<br>京都市右京区西院春日町2</td>
						</tr>
						<tr>
							<th>TEL</th>
							<td>075-312-7665</td>
						</tr>
						<tr>
							<th>FAX</th>
							<td>075-313-8854</td>
						</tr>
						<tr>
							<th>E-mail</th>
							<td><a href="mailto:showa@showashokai.co.jp">showa@showashokai.co.jp</a></td>
						</tr>
						<tr>
							<th>代表者</th>
							<td>代表取締役社長 松田大蔵</td>
						</tr>
						<tr>
							<th>創業</th>
							<td>昭和23年2月(1948年)</td>
						</tr>
						<tr>
							<th>設立</th>
							<td>昭和36年10月(1961年)</td>
						</tr>
						<tr>
							<th>資本金</th>
							<td>82,000,000円</td>
						</tr>
						<tr>
							<th>営業内容</th>
							<td>・ダンボールケース及びダンボール製品の製造<br />
								・美術印刷紙器、POP製作<br />
								・ダンボールパレットの企画製造<br />
								・プラスチックダンボール製品の製作販売<br />
								・梱包・包装機器及び梱包資材の販売<br />
								・商品保管・管理・梱包・発送<br />
								　（トータル及びスポット物流サービス）<br />
								・その他	
							</td>
						</tr>
						<tr>
							<th>事業所</th>
							<td>
								<p>
								■ 関西事業部<br />
								・京都工場 京都市右京区西院春栄町53-2番地<br />
								・城陽工場 京都府城陽市水主北ノ口34番地1<br />
								・滋賀物流 滋賀県米原市顔戸1011-1
								</p>
								<p class="pt20">
								■  北陸事業部<br />
								・福井紙器彩感 福井県あわら市伊井4郷之木1番1
								</p>
								<p class="pt20">
								■  中国事業部<br />
								・岡山工場 岡山県瀬戸内市邑久町豆田1192番
								</p>
								<p class="pt20">
								■  東海事業部<br />
								・三重工場 三重県津市あのつ台4丁目2番4
								</p>
							</td>
						</tr>
						<tr>
							<th>取得</th>
							<td>
								ISO9001-2004 1999.12月(北陸事業部・福井紙器彩感)<br />
								ISO14001-2000 2000.9月(北陸事業部・福井紙器彩感)
							</td>
						</tr>
					</tbody>
				</table>
			</div>		
		</div>					
</div><!-- end primary-row -->
<?php get_template_part('footer'); ?>