<?php get_header(); ?>
    <div class="primary-row clearfix"><!-- begin primary-row -->                    
        <div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
                <div class="pladan-main1">
					<div class="pladan-main1-img"><img src="<?php bloginfo('template_url'); ?>/img/content/pladan_content_img1.png" alt="pladan" /></div>					
					<p class="pladan-main1-text1">耐久性・コストパフォーマンスに優れた</p>
					<p class="pladan-main1-text2">プラダン(プラスチック段ボール)</p>
					<div class="pladan-main1-text3">
						<p>プラダンはポリプロピレン樹脂(PP)製で中空構造(ハーモニカ状)のため、より軽さと強度を両立させコストパフォーマンスに優れた物流・梱包素材で<br />す。 また、耐久性・耐水性・耐熱性・耐薬品性・耐油性に優れ繰り返し使用することができるので、梱包や輸送コストの削減に大きく貢献します。</p>
					</div>					
				</div>
            </div>			          
        </div>        
	</div><!-- end primary-row --> 	
   
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h3 class="pladan-title">昭和商会のプラダンの特徴</h3>
			</div>
		</div> 
		<div class="row pladan-main2 clearfix">
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 pladan-main2-point">
				point1
			</div>
			<div class="col-lg-16 col-md-16 col-sm-16 col-xs-16 pladan-main2-text">
				用途は通箱・仕切り板・養生シート・ディスプレー看板など、優れた汎用性
			</div>
		</div>
		
		<div class="row pladan-main2 clearfix">
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 pladan-main2-point">
				point2
			</div>
			<div class="col-lg-16 col-md-16 col-sm-16 col-xs-16 pladan-main2-text">
				お客様のニーズに合わせ、ご希望寸法で製作するため作業効率UP
			</div>
		</div>
		
		<div class="row pladan-main2 clearfix">
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 pladan-main2-point">
				point3
			</div>
			<div class="col-lg-16 col-md-16 col-sm-16 col-xs-16 pladan-main2-text">
				繰り返し使用できるため、物流経費削減に大きく貢献
			</div>
		</div>
	</div><!-- end primary-row --> 
	
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h3 class="pladan-title">プラダン商品一例</h3>
			</div>
		</div>
		<div class="row mt10 clearfix">
			<div class="col-l-6 col-md-6 col-sm-6 col-xs-6 pladan-main3-sm">
				<div class="pladan-main3">
					<div class="pladan-main3-img"><img src="<?php bloginfo('template_url'); ?>/img/content/pladan_content_img2.png" alt="pladan" /></div>					
					<h4 class="pladan-main3-title">ユニボックスエコL</h4>
					<div class="pladan-main3-text">
						<p>より軽くさらに強い、木型不要の通箱。<br />PP単一素材で製作しており、リサイクルが容易。</p>
					</div>
				</div>
			</div>
			
			<div class="col-l-6 col-md-6 col-sm-6 col-xs-6 pladan-main3-sm">
				<div class="pladan-main3">
					<div class="pladan-main3-img"><img src="<?php bloginfo('template_url'); ?>/img/content/pladan_content_img3.png" alt="pladan" /></div>					
					<h4 class="pladan-main3-title">Z梱</h4>
					<div class="pladan-main3-text">
						<p>折り畳み式の大型コンテナー。四方を折り畳むだけの<br />簡単構造で、返却コストを低減。</p>
					</div>
				</div>
			</div>
			
			<div class="col-l-6 col-md-6 col-sm-6 col-xs-6 pladan-main3-sm">
				<div class="pladan-main3">
					<div class="pladan-main3-img"><img src="<?php bloginfo('template_url'); ?>/img/content/pladan_content_img4.png" alt="pladan" /></div>					
					<h4 class="pladan-main3-title">A式ケース</h4>
					<div class="pladan-main3-text">
						<p>今お使いのダンボールち同等にて使用できます。<br />底部は組底・ワンタッチなどの加工が可能。</p>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row mt30 mb50 clearfix">
			<div class="col-l-6 col-md-6 col-sm-6 col-xs-6 pladan-main3-sm">
				<div class="pladan-main3">
					<div class="pladan-main3-img"><img src="<?php bloginfo('template_url'); ?>/img/content/pladan_content_img5.png" alt="pladan" /></div>					
					<h4 class="pladan-main3-title">O型ケース</h4>
					<div class="pladan-main3-text">
						<p>ワンタッチのため、箱の組み立て、バラシが簡単。<br />積み重ねたまま内容物の出し入れが可能。</p>
					</div>
				</div>
			</div>
			
			<div class="col-l-6 col-md-6 col-sm-6 col-xs-6 pladan-main3-sm">
				<div class="pladan-main3">
					<div class="pladan-main3-img"><img src="<?php bloginfo('template_url'); ?>/img/content/pladan_content_img6.png" alt="pladan" /></div>					
					<h4 class="pladan-main3-title">ハセップラ</h4>
					<div class="pladan-main3-text">
						<p>洗浄可能なプラダン端面シール通箱。<br />シート端面を塞ぐことで、虫・埃・異物混入を防止。<br />食品・医薬などに最適。</p>
					</div>
				</div>
			</div>
			
			<div class="col-l-6 col-md-6 col-sm-6 col-xs-6 pladan-main3-sm">
				<div class="pladan-main3">
					<div class="pladan-main3-img"><img src="<?php bloginfo('template_url'); ?>/img/content/pladan_content_img7.png" alt="pladan" /></div>					
					<h4 class="pladan-main3-title">ユニパネル</h4>
					<div class="pladan-main3-text">
						<p>合板に比べて軽量で軽く、耐久性があります。<br />木屑・ささくれなどの発生がなく、安全で衛生的。<br />シート端面を塞ぐことで、虫・埃・異物混入を防止。</p>
					</div>
				</div>
			</div>
		</div>
	</div><!-- end primary-row -->
	                                                                		                                                          	                                                                                                                                         	                          
<?php get_footer(); ?>