<?php get_header(); ?>
    <div class="primary-row clearfix"><!-- begin container -->                    
        <div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
                <div class="workshop-main1">										
					<h3 class="workshop-main1-title">城陽工場</h3>
					<div class="workshop-main1-text1">
						<p>フレキソ印刷から裁断、接合までを全て自動制御で行うFFG製造ライン(8尺)を配置。ご要望に合わせた段ボール製品を短納期にて製作することが可能です。<br />一般的な段ボール製品の製造のほか、利便性を高めたワンタッチケース等、多様なニーズにお応えしております。</p>
					</div>					
				</div>
            </div>			          
        </div>        
	</div><!-- end primary-row --> 	
   
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-18">
				<h4 class="workshop-title1">街を歩けば</h4>
				<div class="workshop-text1">
					<p>京都府城陽市は山城盆地の中央部に位置し、京都から五里の距離にあることから五里五里の里と呼ばれています。<br />また、「正道官衙遺跡(国の史跡)」や久世神社、水度神社(いずれも本殿が国の重要文化財に指定)など歴史ある建造物も多数あるほか、 現在はサッカーの「京都サンガFC」の練習場。サンガタウン城陽があることでも知られています。</p>
				</div>
				<h5 class="workshop-title2">所在地</h5>
				<div class="workshop-text2">
					<p>610-0118<br />京都府城陽市水住北ノ口34-1<br />TEL.0774-52-2416　FAX.0774-52-0990</p>
				</div>
				
				<h5 class="workshop-title2">敷地</h5>
				<div class="workshop-text2">
					<p>敷地面積：1320㎡<br />延床面積：1956㎡<br />平置倉庫：1210㎡</p>
				</div>
				
				<h5 class="workshop-title2">製造業務内容</h5>
				<div class="workshop-text2">
					<p>FFG製造ライン・平盤ダイカッター製造ライン(8尺)<br />ワンタッチケース製造・一般段ボール製品製造</p>
				</div>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-18">
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/jyoyo_content_top.jpg" alt="jyoyo" /></p>
			</div>
		</div> 		
	</div><!-- end primary-row --> 
	
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h3 class="workshop-slider-title">城陽工場の写真紹介</h3>
				<?php get_template_part('part','factory');?>
			</div>						
		</div> 
	</div><!-- end primary-row --> 
	<div class="workshop-map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d13098.529777634332!2d135.76234465!3d34.8403328!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1z5Lqs6YO95bqc5Z-O6Zm95biC5rC05L2P5YyX44OO5Y-jMzQtMQ!5e0!3m2!1svi!2s!4v1432635007105" width="100%" height="470" frameborder="0" style="border:0"></iframe>
	</div>	                                                          		                                                          	                                                                                                                                         	                          
<?php get_footer(); ?>