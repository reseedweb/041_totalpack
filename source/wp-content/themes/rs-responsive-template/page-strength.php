<?php get_header(); ?>	
    <div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h2 class="strength-title">段ボール・プラダン製造、<br />そして物流でお客様の期待に応える</h2>
			</div>
		</div>
        <div class="row strength-top-content clearfix">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 strength-top-infotext">
                <article class="strength-info">
					<h3 class="strength-top-title">私たちが作りだすパッケージ</h3>					
					<div class="strength-top-text">
						<p>私たちが作りだすパッケージ、それはお客様のニーズを反映する「顔」。<br />
						販売促進効果を高める情報力を持ち、物流の安全を常に確保する安全力を備え、ライフスタイルに合わせて 自在に変化する創造力で作り上げるもの。 形状や大きさ、強度など、お客様のニーズに合わせた一つひとつがオーダーメードともいえる段ボール製品をいかにご提案できるかが 私たちの使命であり、誇りでもあります。</p>
					</div>
				</article>
            </div>			
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 strength-top-infoimg">
				<img src="<?php bloginfo('template_url'); ?>/img/content/strength_top_content_img.jpg" alt="strength" />
			</div>			
        </div>        
	</div><!-- end primary-row --> 
   
	<div class="primary-row strength-content1-bg clearfix"><!-- begin primary-row -->                    		
			<div class="row clearfix">
				<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
					<h3 class="strength-title">各事業所と連携し、短納期・低コストを実現</h3>
				</div>
			</div>
			<div class="row mt20 clearfix">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 strength-content1-sm">
					<div class="strength-content1 clearfix">
						<div class="strength-content1-img"><img src="<?php bloginfo('template_url'); ?>/img/content/strength_conent1_img1.jpg" alt="strength" /></div>					
						<h4 class="strength-content1-title">京都工場</h4>
						<div class="strength-content1-text">
							<p>京都工場では、主にお客様のニーズに合わせた製品サンプルの企画・開発、また段ボール什器・商品パッケージ箱などのPOP・美術印刷紙器の企画・設計等を行っております。</p>
						</div>
					</div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 strength-content1-sm">
					<div class="strength-content1">
						<div class="strength-content1-img"><img src="<?php bloginfo('template_url'); ?>/img/content/strength_conent1_img2.jpg" alt="strength" /></div>					
						<h4 class="strength-content1-title">城陽工場</h4>
						<div class="strength-content1-text">
							<p>フレキソ印刷から裁断、接合までを全て自動制御で行うFFG製造ライン(8尺)を配置。ご要望に合わせた段ボール製品を短納期にて製作することが可能です。</p>
						</div>
					</div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 strength-content1-sm">
					<div class="strength-content1">
						<div class="strength-content1-img"><img src="<?php bloginfo('template_url'); ?>/img/content/strength_conent1_img3.jpg" alt="strength" /></div>					
						<h4 class="strength-content1-title">滋賀物流サービス</h4>
						<div class="strength-content1-text">
							<p>滋賀物流サービスでは、昨今、段ボールよりもさらに軽量で強度もあり、何度も繰り返して使えることからエコの観点でもお客様の注目度の高いプラダン製品を製造しております。</p>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row mt20 mb30 clearfix">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 strength-content1-sm">
					<div class="strength-content1">
						<div class="strength-content1-img"><img src="<?php bloginfo('template_url'); ?>/img/content/strength_conent1_img4.jpg" alt="pladan" /></div>					
						<h4 class="strength-content1-title">福井紙器彩感</h4>
						<div class="strength-content1-text">
							<p>FFG製造ライン(9尺)によって、フレキソ印刷から裁断、接合までを自動制御化。通常よりも大型の段ボール製品の製造が可能です。<br />
							また、木製パレットに代わり注目を集める、リサイクル率の高い段ボールパレットのご要望にも対応。</p>
						</div>
					</div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 strength-content1-sm">
					<div class="strength-content1">
						<div class="strength-content1-img"><img src="<?php bloginfo('template_url'); ?>/img/content/strength_conent1_img5.jpg" alt="strength" /></div>					
						<h4 class="strength-content1-title">岡山工場</h4>
						<div class="strength-content1-text">
							<p>全てを自動制御で行うマシンの中でも他にはあまり類のない大型である、10尺のFFG製造ラインを配置。<br />
							大型ケースの製造を可能にしました。中国・山陽を始めとしたエリア展開により、さらなる短納期化とコスト削減を実現し、 お客様のご要望にさらにお応えしています。</p>
						</div>
					</div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 strength-content1-sm">
					<div class="strength-content1">
						<div class="strength-content1-img"><img src="<?php bloginfo('template_url'); ?>/img/content/strength_conent1_img6.jpg" alt="strength" /></div>					
						<h4 class="strength-content1-title">三重工場</h4>
						<div class="strength-content1-text">
							<p>三重工場では、主にお客様のニーズに合わせた製品サンプルの企画・開発、また段ボール什器・商品パッケージ箱などのPOP・美術印刷紙器の企画・設計等を行っております。<br />
							軽量でありながら優れた強度を持つ段ボールの特性を生かした段ボール製品がここで生み出されます。</p>
						</div>
					</div>
				</div>
			</div>		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="strength-content2 clearfix">
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h2 class="strength-title">ご要望に合わせた製品を企画・開発</h2>
				<div class="strength-content2-text">
					<p>さまざまな分野において活用いただいている段ボール素材。<br />
					だからこそ、よりそれぞれのお客様のニーズに合わせたオンリーワンの製品を提供したいと考えます。</p>
				</div>
				<p class="strength-content2-img"><img src="<?php bloginfo('template_url'); ?>/img/content/strength_conent2_img.png" alt="strength" /></p>
			</div>
		</div>
		</div>
	</div>
	
	<div class="primary-row mt50 clearfix"><!-- begin primary-row -->                    
		<div class="strength-content3 clearfix">
			<div class="row clearfix">
				<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
					<h2 class="strength-title">受注から納品までオーダーエントリーシステムを導入</h2>
					<div class="strength-content3-text">
						<p>出荷に合わせて必要なものを、必要なだけを生産し、円滑に納品するための工場統括管理システムです。</p>					
					</div>
					<p class="strength-content3-img"><img src="<?php bloginfo('template_url'); ?>/img/content/strength_conent3_img.jpg" alt="strength" /></p>
				</div>
			</div>
		</div>
	</div>
	                                                                		                                                          	                                                                                                                                         	                          
<?php get_footer(); ?>