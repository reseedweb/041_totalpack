<?php get_header(); ?>
    <div class="primary-row clearfix"><!-- begin primary-row -->                    
        <div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
                <div class="workshop-main1">										
					<h3 class="workshop-main1-title">福井紙器彩感</h3>
					<div class="workshop-main1-text1">
						<p>FFG製造ライン(9尺)によって、フレキソ印刷から裁断、接合までを自動制御化。通常よりも大型の段ボール製品の製造が可能です。<br />また、木製パレットに代わり注目を集める、リサイクル率の高い段ボールパレットのご要望にも対応。<br />ロット、コスト面でもお客様に貢献しております。</p>
					</div>					
				</div>
            </div>			          
        </div>        
	</div><!-- end primary-row --> 	
   
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-18">
				<h4 class="workshop-title1">街を歩けば</h4>
				<div class="workshop-text1">
					<p>福井県あわら市は、県の北端に位置する芦原町と金津町が合併してできた市で、福井平野から加越大地に広がっています。<br />北陸有数の温泉町として知られる芦原温泉や荒波が勇壮な海食景観を誇る東尋坊が有名で、当事業所のある金津町は古く江戸時代には、 金津宿と呼ばれ、福井藩の関所や奉行所が置かれるなど北陸街道の要所としても知られていました。</p>
				</div>
				<h5 class="workshop-title2">所在地</h5>
				<div class="workshop-text2">
					<p>〒919-0614<br />福井県あわら市伊井4郷之木1-1<br />TEL.0776-73-5250　FAX.0776-73-5252</p>
				</div>
				
				<h5 class="workshop-title2">敷地</h5>
				<div class="workshop-text2">
					<p>敷地面積：11387㎡<br />延床面積：3293㎡<br />平置倉庫：750㎡</p>
				</div>
				
				<h5 class="workshop-title2">製造業務内容</h5>
				<div class="workshop-text2">
					<p>FFG製造ライン(9尺)・平盤ダイカッター製造ライン<br />段ボールパレット製造<br />一般段ボール製品製造</p>
				</div>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-18">
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/hukui_content_top.jpg" alt="hukui" /></p>
			</div>
		</div> 		
	</div><!-- end primary-row --> 
	
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h3 class="workshop-slider-title">福井紙器彩感の写真紹介</h3>
				<?php get_template_part('part','factory');?>
			</div>						
		</div> 
	</div><!-- end primary-row --> 
	<div class="workshop-map" id="map_area">
		<script>
			$(function() {
				map_insert (36.199603,136.244523);
			});
		</script>
	</div>	                                                          		                                                          	                                                                                                                                         	                          
<?php get_footer(); ?>