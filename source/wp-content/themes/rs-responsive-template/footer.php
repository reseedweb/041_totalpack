							</main><!-- end primary -->
                        </div><!-- end col -->
                        <div class="col-xs-18" id="sibar-sp"><!-- begin coll -->
                            <aside id="sidebar">
                                <?php if(is_page('blog') || is_category() || is_single()) : ?>
								<?php
									$queried_object = get_queried_object();                                
									$sidebar_part = 'blog';
									if(is_tax() || is_archive()){                                    
										$sidebar_part = '';
									}                               
									if($queried_object->post_type != 'post' && $queried_object->post_type != 'page'){
										$sidebar_part = '';
									}   
									if($queried_object->taxonomy == 'category'){                                    
										$sidebar_part = 'blog';
									}                 
								?>
								<?php get_template_part('sidebar',$sidebar_part); ?>  
								<?php else: ?>
									<?php get_template_part('sidebar'); ?>  
								<?php endif; ?> 
                            </aside><!-- end sidebar -->
                        </div><!-- end col -->                   
                    </div><!-- end row -->
                </div><!-- end container -->
            </section><!-- end content -->
           
			<footer><!-- begin footer -->
				<div id="footer"><!-- #footer -->            
					<div class="container"><!-- begin container -->
						<div class="row"><!-- begin row -->
							<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18"><!-- begin col -->
								<div class="footer-title">トータルパック株式会社</div>
								<div class="footer-info">
									<p>〒660-0087 兵庫県尼崎市平左衛門町18番地34<br />TEL：06-6416-6721　FAX:06-6419-6766<br />Copyright © 2015 トータルパック株式会社 All Rights Reserved.</p>
								</div>
							</div><!-- end col -->
						</div><!-- end row -->
					</div><!-- end container -->        
				</div><!-- /#footer -->
			</footer><!-- end footer -->
                        
        </div><!-- end wrapper -->
        <?php wp_footer(); ?>		
    </body>
</html>