<?php get_header(); ?>
<div class="primary-row">
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	<h2><span><?php the_title(); ?></span></h2>
 	<div class="post-row-content clearfix">
		<div class="post-row-meta">
			<i class="fa fa-clock-o"></i><?php the_time('l, F jS, Y'); ?>
			<i class="fa fa-tags"></i><span class="blog-category"><?php the_category(' , ', get_the_id()); ?></span>
			<i class="fa fa-user"></i><span style="color:red;"><?php the_author_link(); ?></span>
		</div>    
    	<div class="blog-content"><?php the_content(); ?></div>
    </div><!-- end post-row-content -->
    <?php endwhile; endif; ?>       
    <div class="navigation">
		<?php if( get_previous_post() ): ?>
		<div style="float:left;"><?php previous_post_link('%link', '« %title'); ?></div>
		<?php endif;
		if( get_next_post() ): ?>
		<div style="float:right;"><?php next_post_link('%link', '%title »'); ?></div>
		<?php endif; ?>
    </div><!--  end navigation -->	
</div><!-- end primary-row -->
<?php get_footer(); ?>