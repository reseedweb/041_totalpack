			<nav>
				<div id="menu-toggle"><!-- begin menu-toggle -->
					<div class="globalNavi-bg"><!-- begin globalNavi-bg -->
						<div class="container"><!-- begin container -->														
								<div class="globalNavi row clearfix">
									<ul>
										<li><a href="<?php bloginfo('url'); ?>/strength">会社の強み</a></li>										
										<li><a href="<?php bloginfo('url'); ?>/">商品紹介</a>
											<ul>
												<li><a href="<?php bloginfo('url'); ?>/cardboard">段ボール</a></li>
												<li><a href="<?php bloginfo('url'); ?>/pladan">プラダン</a></li>
												<li><a href="<?php bloginfo('url'); ?>/pallet">段ボールパレット</a></li>
												<li><a href="<?php bloginfo('url'); ?>/package">パッケージ</a></li>
											</ul>
										</li>										
										<li><a href="<?php bloginfo('url'); ?>/">工場紹介</a>
											<ul>
												<li><a href="<?php bloginfo('url'); ?>/kyoto">京都工場</a></li>
												<li><a href="<?php bloginfo('url'); ?>/jyoyo">城陽工場</a></li>
												<li><a href="<?php bloginfo('url'); ?>/shiga">滋賀物流サービス</a></li>
												<li><a href="<?php bloginfo('url'); ?>/hukui">福井紙器彩感</a></li>
												<li><a href="<?php bloginfo('url'); ?>/okayama">岡山工場</a></li>
												<li><a href="<?php bloginfo('url'); ?>/mie">三重工場</a></li>												
											</ul>
										</li>
										<li><a href="<?php bloginfo('url'); ?>/company">会社紹介</a></li>										
										<li><a href="<?php bloginfo('url'); ?>/contact">お問合せ</a></li>																				
									</ul>
								</div>                            							
						</div><!-- end container -->
					</div><!-- end globalNavi-bg -->					      
				</div><!-- end menu-toggle -->
			</nav><!-- end nav -->