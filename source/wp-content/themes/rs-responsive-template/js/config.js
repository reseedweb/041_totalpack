var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
function is_mobile(){
    var screen_type = $('#screen_type').css('content');       
    if(
        screen_type === 'lg' || screen_type === 'md' || screen_type === 'sm' // chrome
        ||
        screen_type === '"lg"' || screen_type === '"md"' || screen_type === '"sm"' //firefox
        ||
        screen_type === undefined
    ) return false;
    return true;    
}
function get_screen_type(){
    var screen_type = $('#screen_type').css('content');    
    switch(screen_type){
        case undefined :        
            return 'ie';        
        case 'lg':
            return 'lg';        
        case '"md"':
        case 'md':
            return 'md';
        case 'sm':
        case '"sm"':
            return 'sm';
        case 'xs':
        case '"xs"':
            return 'xs';                     
        default:
            return 'lg';
    }

}
function map_insert (x,y) {
    google.maps.event.addDomListener(window,'load',function(){
      var mapOptions = {
        zoom: 15,
        center: new google.maps.LatLng(x,y),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false
      };
      var mp = new google.maps.Map(document.getElementById('map_area'), mapOptions);
      var marker = new google.maps.Marker({
          position: mapOptions.center,
          map: mp
      });
    });
}
$(document).ready(function(){
    var sp_img = $('#slider-banner').children('img');
    $('#wrapper').addClass(get_screen_type()); 

    $( window ).resize(function(){
        $('#wrapper').attr('class', get_screen_type());
    });

    $(window).setBreakpoints({
        distinct: true,
        breakpoints: [ 1, 640 ]
    });
    $(window).bind('enterBreakpoint640',function() {
        sp_img.each(function() {
            $(this).attr('src', $(this).attr('src').replace('_sp', '_pc'));
        });
    });
    $(window).bind('enterBreakpoint1',function() {
        sp_img.each(function() {
            $(this).attr('src', $(this).attr('src').replace('_pc', '_sp'));
        });
    });
});

/*----- js navi -----*/
$(window).on('load resize', function(){
    var w = $(window).width();
    var x = 500;
    var sp_x = 480;
    $("#content").waypoint({
        handler: function(direction) {
            if (direction == 'down' && w > x) {
                $("#top-header-info").slideUp();
            }else if(direction == 'up' && w > x){
                $("#top-header-info").slideDown();
            }
        }
    });                     
});

$(document).ready(function() {      
    // Append the mobile icon nav
        $('.sp_header').append($('<div class="nav-mobile"></div>'));
        
        // Add a <span> to every .nav-item that has a <ul> inside
        $('.nav-item').has('ul').prepend('<span class="nav-click"><i class="nav-arrow"></i></span>');
        
        // Click to reveal the nav
        $('.nav-mobile').click(function(){
            $('.nav-list').toggle();
        });

        $('.nav-list').on('click', '.nav-itemclr', function(){
            // Toggle the nested nav
            $(this).children('ul').slideToggle();     
        });     
    /*----- js top-main-slider -----*/
    $('.top-main-slider').slick({
        dots: false,
        infinite: true,
        slidesToScroll: 1,
        centerMode: true,
        variableWidth: true,
        autoplay: true,
        autoplaySpeed: 2000,
        pauseOnHover:false,
        arrows:false
    });  
    
    /*----- js workshop -----*/
    $('.workshop-slider').slick({
        dots: false,
        infinite: true,
        arrows:false,
        speed: 300,
        autoplay: true,
        accessibility: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        centerMode: true,
        adaptiveHeight: true,
        responsive:[{
        breakpoint: 480,
        settings:{
        slidesToShow: 1,
        slidesToScroll: 1
        }
        }]
    });
      
    /*----- js contact -----*/
    $('#zip').change(function(){                    
        //AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
        AjaxZip3.zip2addr(this,'','pref','addr');
    });
	
	$('#sideMenu > ul > li > a').click(function() {
		  $('#sideMenu li').removeClass('active');
		  $(this).closest('li').addClass('active');    
		  var checkElement = $(this).next();
		  if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
			$(this).closest('li').removeClass('active');
			checkElement.slideUp('normal');
		  }
		  if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
			$('#sideMenu ul .sub-menu:visible').slideUp('normal');
			checkElement.slideDown('normal');
		  }
		  if($(this).closest('li').find('ul').children().length == 0) {
			return true;
		  } else {
			return false;    
		  }        
		});
}); 

$( window ).resize(function() {                    
    if(is_mobile()){                                        
		$('.add-sp').removeClass('container-head');                        
    }else{                        
        $('.add-sp').addClass('container-head');                        
    }                
});  
