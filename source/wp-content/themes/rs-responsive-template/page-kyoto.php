<?php get_header(); ?>
    <div class="primary-row clearfix"><!-- begin primary-row -->                    
        <div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
                <div class="workshop-main1">										
					<h3 class="workshop-main1-title">京都工場</h3>
					<div class="workshop-main1-text1">
						<p>京都工場では、主にお客様のニーズに合わせた製品サンプルの企画・開発、また、段ボール什器・商品パッケージ箱などのPOP・美術印刷紙器の企画・設計等を行っております。<br />
						軽量でありながら優れた強度を持つ段ボールの特性を生かした段ボール製品がここで生み出されます。</p>
					</div>					
				</div>
            </div>			          
        </div>        
	</div><!-- end primary-row --> 	
   
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-18">
				<h4 class="workshop-title1">街を歩けば</h4>
				<div class="workshop-text1">
					<p>794年、京都は平安京として日本の首都と定められ、以後、政治、文化の中心として栄えました。<br />
					「京の都」と呼ばれたことから、京都という固有名詞となったこの地には古い歴史が刻まれています。 当社発祥の地である「西院」の町は、古くは「続日本記」や「類聚国史」にも登場。 平安京の頃。左比大路(現在の佐井通り)と四条大路の北側に淳和天皇の離宮「淳和院」があり、御所から見て西の方角に位置したことから この付近一帯を「西院」と呼ぶようになったと言われています。</p>
				</div>
				<h5 class="workshop-title2">所在地</h5>
				<div class="workshop-text2">
					<p>615-0005<br />京都市右京区西院春栄町53-2<br />TEL.075-312-7968　FAX.075-313-8854</p>
				</div>
				
				<h5 class="workshop-title2">敷地</h5>
				<div class="workshop-text2">
					<p>敷地面積：464㎡<br />延床面積：588㎡<br />平置倉庫：290㎡</p>
				</div>
				
				<h5 class="workshop-title2">製造業務内容</h5>
				<div class="workshop-text2">
					<p>サンプル等企画・開発<br />POP・美術印刷紙器企画</p>
				</div>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-18">
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/kyoto_content_top.jpg" alt="kyoto" /></p>
			</div>
		</div> 		
	</div><!-- end primary-row --> 
	
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h3 class="workshop-slider-title">昭和商会のプラダンの特徴</h3>
				<?php get_template_part('part','factory');?>
			</div>						
		</div> 
	</div><!-- end primary-row --> 
	<div class="workshop-map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3268.0430402423663!2d135.7284358!3d35.005627700000005!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x600107b40825b5a1%3A0x9d024a18c7b9ee4d!2zMiBTYWlpbmthc3VnYWNoxY0sIFVrecWNLWt1LCBLecWNdG8tc2hpLCBLecWNdG8tZnUgNjE1LTAwMTUsIEphcGFu!5e0!3m2!1sen!2s!4v1431508007560" width="100%" height="470" frameborder="0" style="border:0"></iframe>
	</div>	                                                          		                                                          	                                                                                                                                         	                          
<?php get_footer(); ?>