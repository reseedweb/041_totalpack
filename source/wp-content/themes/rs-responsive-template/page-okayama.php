<?php get_header(); ?>

    <div class="primary-row clearfix"><!-- begin primary-row -->                    
        <div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
                <div class="workshop-main1">										
					<h3 class="workshop-main1-title">岡山工場</h3>
					<div class="workshop-main1-text1">
						<p>全てを自動制御で行うマシンの中でも他にはあまり類のない大型である、10尺のFFG製造ラインを配置。<br />大型ケースの製造を可能にしました。中国・山陽を始めとしたエリア展開により、さらなる短納期化とコスト削減を実現し、 お客様のご要望にさらにお応えしています。</p>
					</div>					
				</div>
            </div>			          
        </div>        
	</div><!-- end primary-row --> 	
   
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-18">
				<h4 class="workshop-title1">街を歩けば</h4>
				<div class="workshop-text1">
					<p>岡山県瀬戸内市は、牛窓町、邑久町、長船町の合併によって誕生した市で、豊かな自然と歴史が 調和する交流と創造の都市です。当事業所のある邑久町は、西端には吉井川が流れ、中央部に 千町川、その間には千町平野が広がっています、また東部は瀬戸内海に面した広陵地と長島などの島々があり 、竹久夢二誕生の地として多くの人に親しまれています。</p>
				</div>
				<h5 class="workshop-title2">所在地</h5>
				<div class="workshop-text2">
					<p>〒701-4254<br />岡山県瀬戸内市邑久町豆田1192<br />TEL.0869-24-1450　FAX.0869-24-1440</p>
				</div>
				
				<h5 class="workshop-title2">敷地</h5>
				<div class="workshop-text2">
					<p>敷地面積：2135㎡<br />延床面積：1060㎡<br />平置倉庫：660㎡</p>
				</div>
				
				<h5 class="workshop-title2">製造業務内容</h5>
				<div class="workshop-text2">
					<p>FFG製造ライン(10尺)<br />一般段ボール製品製造<br />一大型ケース製造</p>
				</div>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-18">
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/okayama_content_top.jpg" alt="okayama" /></p>
			</div>
		</div> 		
	</div><!-- end primary-row --> 
	
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h3 class="workshop-slider-title">岡山工場の写真紹介</h3>
				<?php get_template_part('part','factory');?>
			</div>						
		</div> 
	</div><!-- end primary-row --> 
	<div class="workshop-map" id="map_area">
		<script>
			$(function() {
				map_insert (34.686166,134.082284);
			});
		</script>
	</div>	                                                          		                                                          	                                                                                                                                         	                          
<?php get_footer(); ?>