<?php get_header(); ?>
    <div class="primary-row clearfix"><!-- begin primary-row -->                    
        <div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
                <div class="pallet-main1">
					<div class="pallet-main1-img"><img src="<?php bloginfo('template_url'); ?>/img/content/pallet_content_img1.png" alt="pallet" /></div>					
					<p class="pallet-main1-text1">充分な強度とリサイクル率の高い</p>
					<p class="pallet-main1-text2">ショウワ段ボールパレット</p>
					<div class="pallet-main1-text3">
						<p>現在、物流パレットは、全国に4億枚から5億枚が使用されていると言われています。<br />
							地球環境問題、環境規制が世界中で大きな反響となっております。<br />
							木製パレットにつきましては、森林資源の伐採削減、リサイクル化、環境ISO、燻蒸処理、コストの低減等、多くの問題を残しております。<br />
							物流パレットにリサイクル率の高いショウワダンボールパレットを使用することはグリーン購入につながります。</p>
					</div>					
				</div>
            </div>			          
        </div>        
	</div><!-- end primary-row --> 	
   
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h3 class="pallet-title">パレットの強度を保つショウワブロック</h3>
			</div>
		</div>
        <div class="row clearfix">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 pallet-main2-sm">
                <article class="pallet-main2">
					<div class="pallet-main2-point">point1</div>					
					<h4 class="pallet-main2-title">偏荷重の弱さを克服</h4>
					<div class="pallet-main2-text">
						<p>十分に強度を維持した上、ケタとデッキの結合面を広く支持することで、偏荷重の弱さを克服しました。</p>						
					</div>
				</article>
            </div>			
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 pallet-main2-sm">
                <article class="pallet-main2">
					<div class="pallet-main2-point">point2</div>					
					<h4 class="pallet-main2-title">自由なサイズ設計</h4>
					<div class="pallet-main2-text">
						<p>「ショウワブロック」を自由に組み合わせる事で、積載物の形状、重量、パレットへの積み方などに素早く対応し、 どんなサイズでもつくれるようになりました。</p>						
					</div>
				</article>
            </div>			
        </div> 

		<div class="row clearfix">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 pallet-main2-sm">
                <article class="pallet-main2">
					<div class="pallet-main2-point">point3</div>					
					<h4 class="pallet-main2-title">材料面での低コスト化</h4>
					<div class="pallet-main2-text">
						<p>「ショウワブロック」で軽量化し、材料面での低コスト化にも成功しました。(積層品の約3/1)</p>						
					</div>
				</article>
            </div>			
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 pallet-main2-sm">
                <article class="pallet-main2">
					<div class="pallet-main2-point">point4</div>					
					<h4 class="pallet-main2-title">小ロットから大ロットまで</h4>
					<div class="pallet-main2-text">
						<p>とりわけ生産面では、組立と圧着作業を簡素化し、小ロットから大ロットまで自由自在に生産し、価格面にも大きく貢献出来るようになりました。</p>						
					</div>
				</article>
            </div>			
        </div>   
	</div><!-- end primary-row --> 
	
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h3 class="pallet-title">ショウワ段ボールパレット仕様例</h3>
			</div>
		</div>
		<div class="row mt10 mb50 clearfix">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pallet-main3-sm">
				<div class="pallet-main3">
					<div class="pallet-main3-img"><img src="<?php bloginfo('template_url'); ?>/img/content/pallet_content_img2.jpg" alt="pallet" /></div>					
					<h4 class="pallet-main3-title">R4-16型16A(天板・底板)四方差し</h4>
					<div class="pallet-main3-text">
						<p>ショウワブロック：C5(200x200x95)9本使用<br />平面圧縮強度:8,010kgf(300kgx3段積程度)</p>
					</div>
				</div>
			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pallet-main3-sm">
				<div class="pallet-main3">
					<div class="pallet-main3-img"><img src="<?php bloginfo('template_url'); ?>/img/content/pallet_content_img3.jpg" alt="pallet" /></div>					
					<h4 class="pallet-main3-title">R4-266型16A(天板・天補強板・底板)四方差し</h4>
					<div class="pallet-main3-text">
						<p>ショウワブロック：A3(150x300x95)2本使用<br />ショウワブロック：C3(200x300x95)4本使用<br />ショウワブロック：C5(200x200x95)4本使用<br />平面圧縮強度:11,740kgf(500kgx3段積程度)</p>
					</div>
				</div>
			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pallet-main3-sm">
				<div class="pallet-main3">
					<div class="pallet-main3-img"><img src="<?php bloginfo('template_url'); ?>/img/content/pallet_content_img4.jpg" alt="pallet" /></div>					
					<h4 class="pallet-main3-title">R4-277型277A(天板・天補強板・底補強板・底板)二方差し</h4>
					<div class="pallet-main3-text">
						<p>ショウワブロック：A3(150x300x95)2本使用<br />ショウワブロック：C3(200x300x95)4本使用<br />ショウワブロック：C5(200x200x95)4本使用<br />平面圧縮強度:11,740kgf(500kgx3段積程度)</p>
					</div>
				</div>
			</div>
		</div>
	</div><!-- end primary-row -->
	                                                                		                                                          	                                                                                                                                         	                          
<?php get_footer(); ?>