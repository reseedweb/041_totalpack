<?php get_header(); ?>
    <div class="primary-row clearfix"><!-- begin container -->                    
        <div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
                <div class="greeting-content clearfix">										
					<h3 class="greeting-content-title">理想と創造を実現致します</h3>
					<div class="greeting-content-text">
						<p>私たち株式会社昭和商会は、１９４８年にダンボールケース・ダンボール製品の製造販売を営業目的として創業いたしました。<br />
						   おかげさまでお客様のあたたかいご支援の下、京都本社、京都城陽工場、福井紙器彩感、岡山工場、滋賀物流サービス、三重工場と、関西をはじめ、 北陸、中国、山陽、東海、中部地域でお客様のニーズにお答えしております。<br />
						   また、少しでも販売促進のお役に立てていただけるよう、機能的で経済性に富んだパッケージのご提案を行い、 製品保護・保管・輸送まで幅広い営業活動を行っております。<br />
						   今後も更なる一歩を目指し、今まで以上に社会に貢献するとともに、環境にやさしいパッケージやディスプレイ製品を理想と想像をイメージし、お客様のニーズにお応え出来るように邁進いたします。</p>
					</div>					
				</div>
            </div>			          
        </div>        
	</div><!-- end primary-row --> 	                                                    		                                                          	                                                                                                                                         	                          
<?php get_footer(); ?>