<div id='sideMenu'>
	<ul>
	   <li><a href="<?php bloginfo('url'); ?>/strength"><span>会社の強み</span></a></li>
	   <li class='has-sub'><a href="<?php bloginfo('url'); ?>/#"><span>商品紹介</span></a>
		  <ul class="sub-menu">
				<li><a href="<?php bloginfo('url'); ?>/cardboard"><span>段ボール</span></a></li>
				<li><a href="<?php bloginfo('url'); ?>/pladan"><span>プラダン</span></a></li>
				<li><a href="<?php bloginfo('url'); ?>/pallet"><span>段ボールパレット</span></a></li>
				<li><a href="<?php bloginfo('url'); ?>/package"><span>パッケージ</span></a></li>         
		  </ul>
	   </li>
	   <li class='has-sub'><a href='#'><span>工場紹介</span></a>
		  <ul class="sub-menu">
			 <li><a href="<?php bloginfo('url'); ?>/kyoto"><span>京都工場</span></a></li>
			 <li><a href="<?php bloginfo('url'); ?>/jyoyo"><span>城陽工場</span></a></li>
			 <li><a href="<?php bloginfo('url'); ?>/shiga"><span>滋賀物流サービス</span></a></li>
			 <li><a href="<?php bloginfo('url'); ?>/hukui"><span>福井紙器彩感</span></a></li>
			 <li><a href="<?php bloginfo('url'); ?>/okayama"><span>岡山工場</span></a></li>
			 <li><a href="<?php bloginfo('url'); ?>/mie"><span>三重工場</span></a></li>		 
		  </ul>
	   </li>
	   <li><a href="<?php bloginfo('url'); ?>/company"><span>会社紹介</span></a></li>
	   <li><a href="<?php bloginfo('url'); ?>/contact"><span>お問合せ</span></a></li>
	</ul>
</div>
