<?php get_header(); ?>
    <div class="primary-row clearfix"><!-- begin primary-row -->                    
        <div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
                <div class="workshop-main1">										
					<h3 class="workshop-main1-title">滋賀物流サービス</h3>
					<div class="workshop-main1-text1">
						<p>滋賀物流サービスでは、昨今、段ボールよりもさらに軽量で強度もあり、何度も繰り返して使えることからエコの観点でもお客様の注目度も高いプラダン製品を製造しております。<br />また、物流の要である倉庫・輸送業において、コストダウンや納入率アップでお客様をサポートします。</p>
					</div>					
				</div>
            </div>			          
        </div>        
	</div><!-- end primary-row --> 	
   
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-18">
				<h4 class="workshop-title1">街を歩けば</h4>
				<div class="workshop-text1">
					<p>滋賀県米原市は、日本百名山の一つである伊吹山とその南に雲仙山がそびえ、清流・姉川や天の川が琵琶湖へと注ぎこむ場所にあります。<br />古くから、近畿・東海・北陸を結ぶ交通の要所である中山道や北陸街道の宿場町としても栄え、緑と水に囲まれた自然豊かな地域であり 、戦国時代を代表する豊臣秀吉や石田三成らが活躍した歴史の舞台としても知られています。</p>
				</div>
				<h5 class="workshop-title2">所在地</h5>
				<div class="workshop-text2">
					<p>〒521-0072<br />滋賀県米原市顔戸1011-1<br />TEL.0749-52-3055　FAX.0749-52-3009</p>
				</div>
				
				<h5 class="workshop-title2">敷地</h5>
				<div class="workshop-text2">
					<p>敷地面積：8580㎡<br />延床面積：6150㎡+自動立体倉庫5227㎡<br />物流設備：自動倉庫:4320棚(1100～1100パレット4320P収納)<br />平置倉庫：4890㎡+自動立体倉庫5227㎡<br />保冷庫A：温度設定3+-2度<br />保冷庫B：温度設定6+-2度</p>
				</div>
				
				<h5 class="workshop-title2">製造業務内容</h5>
				<div class="workshop-text2">
					<p>商品保管・管理・梱包・発送(トータル及びスポット物流サービス)<br />販促企画等の販売支援(デザイン・内外装のご提案)<br />カタログ通販及びネット通販等の発送業務<br />段ボールケース等の梱包・包装ツールから段ボールパレットの製作</p>
				</div>
			</div>
			<div class="col-lg6 col-md-6 col-sm-6 col-xs-12">
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/shiga_content_top.jpg" alt="shiga" /></p>
			</div>
		</div> 		
	</div><!-- end primary-row --> 
	
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h3 class="workshop-slider-title">滋賀工場の写真紹介</h3>
				<?php get_template_part('part','factory');?>
			</div>						
		</div> 
	</div><!-- end primary-row --> 
	<div class="workshop-map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3254.6951719004273!2d136.29851969999999!3d35.3383904!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60022c644ef740cd%3A0x342b477e335fc7a1!2s1011-1+G%C5%8Ddo%2C+Maibara-shi%2C+Shiga-ken+521-0072%2C+Japan!5e0!3m2!1svi!2s!4v1432635096843" width="100%" height="470" frameborder="0" style="border:0"></iframe>
	</div>	                                                          		                                                          	                                                                                                                                         	                          
<?php get_footer(); ?>