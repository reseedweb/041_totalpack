<?php get_header(); ?>
    <div class="primary-row clearfix"><!-- begin primary-row -->                                                                                       
            <div class="row clearfix">
            	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-9">
                	<article class="top-main1 clearfix">
						<h2 class="top-main1-title">段ボール製品</h2>
						<div class="top-main1-img"><img src="<?php bloginfo('template_url'); ?>/img/top/top_main1_img1.jpg" alt="段ボール製品" /></div>
						<div class="top-main1-text">素材はもちろん軽くて耐久性に富んだプラスチック段ボール</div>
					</article>
				</div>			
			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-9">
             			<article class="top-main1 clearfix">
					<h2 class="top-main1-title">印刷紙器・紙製品</h2>
					<div class="top-main1-img"><img src="<?php bloginfo('template_url'); ?>/img/top/top_main1_img2.jpg" alt="印刷紙器・紙製品" /></div>
					<div class="top-main1-text">充分な強度とリサイクル率の高いショウワ段ボールパレット</div>
				</article>
			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-9">
             	<article class="top-main1 clearfix">
					<h2 class="top-main1-title">包装・梱包資材</h2>
					<div class="top-main1-img"><img src="<?php bloginfo('template_url'); ?>/img/top/top_main1_img3.jpg" alt="包装・梱包資材" /></div>
					<div class="top-main1-text">ご要望に合わせた製品を企画・開発</div>
				</article>
			</div>
			
			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-9">
             	<article class="top-main1 clearfix">
					<h2 class="top-main1-title">詰合せ・梱包作業</h2>
					<div class="top-main1-img"><img src="<?php bloginfo('template_url'); ?>/img/top/top_main1_img4.jpg" alt="詰め合わせ・梱包作業" /></div>
					<div class="top-main1-text">大型段ボールと物流パレットでお客様のニーズと環境に貢献</div>
				</article>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-9">
             	<article class="top-main1 clearfix">
					<h2 class="top-main1-title">規格段ボール製品</h2>
					<div class="top-main1-img"><img src="<?php bloginfo('template_url'); ?>/img/top/top_main1_img5.jpg" alt="規格段ボール製品" /></div>
					<div class="top-main1-text">大型段ボールと物流パレットでお客様のニーズと環境に貢献</div>
				</article>
			</div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-9">
             	<article class="top-main1 clearfix">
					<h2 class="top-main1-title">6Pステップ</h2>
					<div class="top-main1-img"><img src="<?php bloginfo('template_url'); ?>/img/top/top_main1_img6.jpg" alt="6Pステップ" /></div>
					<div class="top-main1-text">大型段ボールと物流パレットでお客様のニーズと環境に貢献</div>
				</article>
            </div>
        </div>        
	</div>
	
    <div class="primary-row clearfix"><!-- begin primary-row -->                                                                               
        <div class="row clearfix">
            <div class="col-md-18">
                <p class="top-status">四角の中に安全・安心を詰め込んだCube X<br/>それが、私たちの提案するパッケージング・ソリューションです。</p>
            </div>
        </div>        
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
        <div class="row top-main2-content clearfix">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 top-main2-infotext">
                <article class="top-main2">
					<h2 class="top-main2-title">私たちが作りだすパッケージ</h2>					
					<div class="top-main2-text">
						<p>私たちが作りだすパッケージ、それはお客様のニーズを反映する「顔」。<br />販売促進効果を高める情報力を持ち、物流の安全を常に確保する安全力を備え、ライフスタイルに合わせて 自在に変化する創造力で作り上げるもの。 形状や大きさ、強度など、お客様のニーズに合わせた一つひとつがオーダーメードともいえる段ボール製品をいかにご提案できるかが 私たちの使命であり、誇りでもあります。</p>
					</div>
				</article>
            </div>			
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 top-main2-infoimg">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_main2_img1.jpg" alt="top" />
			</div>			
        </div>        
	</div><!-- end primary-row --> 
	
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
        <div class="row clearfix">
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 top-main2-infoimg">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_main2_img2.jpg" alt="top" />
			</div>	
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 top-main2-infotext">
                <article class="top-main2">
					<h2 class="top-main2-title">各事業所と連携し、短納期・低コストを実現</h2>					
					<div class="top-main2-text">
						<p>全営業所の統括を行っている京都本社。本社と各工場はデータベースで連動し、稼働状況等を一覧できるように。<br />また、工場ごとに生産する製品を分散しているため、効率よく生産を行えるよう本社で一括管理。<br />スピード化を図るため、京都工場以外受発注を各工場ごとに直接行い、その報告を受けるなど、短納期に対応できる体制づくりも担います。</p>
					</div>
				</article>
			</div>						
        </div>        
	</div><!-- end primary-row --> 
	
	<div class="primary-row clearfix"><!-- begin primary-row -->                                                                         
		<div class="row clearfix">
			<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h2 class="top-h2">お知らせ<span class="top-rss"><a href="<?php bloginfo('url'); ?>">RSS</a></span></h2>
			</div>					
		</div>
		<div class="row clearfix">
				<?php
					//http://www.wpbeginner.com/beginners-guide/what-why-and-how-tos-of-creating-a-site-specific-wordpress-plugin/
					$loop = new WP_Query('showposts=5&orderby=ID&order=DESC');
					if($loop->have_posts()):
				?>
				<ul class="top-blog">
				<?php while($loop->have_posts()): $loop->the_post(); ?>
					<li>
						<a href="<?php the_permalink(); ?>">	
							<div class="top-blog-info">
								<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 top-blog-date"><?php the_time('Y/m/d'); ?></div>
								<div class="col-lg10 col-md-10 col-sm-12 col-xs-12 top-blog-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>																	
								<i class="fa fa-angle-right"></i>
							</div>
						</a>
					</li>                              
				<?php endwhile; wp_reset_postdata();?>                        
				<?php else: ?>
					<li>No recent posts yet!</li>
				<?php endif; ?>		
				</ul>	 			
		</div>
		<div class="row clearfix">
			<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<div class="top-more-blog"><a href="<?php bloginfo('url'); ?>">お知らせ一覧</a></div>
			</div>			
		</div>
	</div><!-- end primary-row --> 
	
	<div id="top-bottom" class="primary-row clearfix"><!-- begin primary-row -->                                                                         
		<article class="top-main3">
			<div class="top-main3-content">				
					<div class="row clearfix">
						<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">											
							<div class="top-main3-title">各事業部と連携し、短納期・低コストを実現</div>
							<div class="top-main3-text">
								<p>全営業所の統括を行っている京都本社。本社と各工場はデータベースで連動し、稼働状況を一覧できるように。<br />また、工場ごとに生産する製品を分散しているため、効率よく生産を行えるよう本社で一括管理。<br />スピード化を図るため、京都工場以外は受発注を各工場ごとに直接行い、その報告を受けるなど、短納期に対応できる体制づくりも担います。</p>
							</div>
						</div>					
					</div>				
			</div>					
		</article>
		
		<article class="top-main4">
			
				<div class="row clearfix">
					<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
						<div class="top-main4-title">受注から納品までオーダーエントリーシステムを導入</div>
						<div class="top-main4-text">
							<p>出荷に合わせて必要なものを、必要なだけを生産し、円滑に納品するための工場統括管理システムです。</p>
						</div>						
					</div>				
				</div>		
			<div class="top-main-slider">
				<?php	
					$posts = get_posts(array(
						'post_type'=> 'top',				
					));?>			
					
					<?php
					$i = 0;
					foreach($posts as $post):
					?>	
					<?php $i++; ?>
						<div>
							<?php 
							if ( has_post_thumbnail() ) {
								$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
								echo '<a rel="lightbox" href="' . $large_image_url[0] . '" title="' . the_title_attribute( 'echo=0' ) . '" >';
								echo '<img src="'. $large_image_url[0].'" title="' . the_title_attribute( 'echo=0' ) . '">';
								echo '</a>';
							}
							?>
						</div><!-- end item -->            
				<?php endforeach; ?>
				
			</div>
		</article>		
		
		<div class="workshop-map" id="map_area">
			<script>
				$(function() {
					map_insert (35.005553,135.728455);
				});
			</script>
		</div>	             
	</div><!-- end primary-row -->
	                                                                		                                                          	                                                                                                                                         	                          
<?php get_footer(); ?>