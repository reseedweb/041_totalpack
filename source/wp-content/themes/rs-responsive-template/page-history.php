<?php get_template_part('header'); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="row clearfix">
			<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h3 class="h3-title">会社沿革</h3>								
				<table class="history-table-content">
					<tbody>
						<tr>
							<th>1961年10月</th>
							<td>株式会社 昭和商会設立</td>
						</tr>
						<tr>
							<th>1976年10月</th>
							<td>関西事業部・城陽工場稼動</td>
						</tr>
						<tr>
							<th>1986年9月</th>
							<td>本社工場建替え落成</td>
						</tr>
						<tr>
							<th>1986年10月</th>
							<td>コーポレートマーク(サイン)に対し第4回京都美観風致賞受賞</td>
						</tr>
						<tr>
							<th>1990年3月</th>
							<td>山崎紙器株式会社設立</td>
						</tr>
						<tr>
							<th>1990年4月</th>
							<td>滋賀県彦根市新海浜に保養研修所を開設</td>
						</tr>
						<tr>
							<th>1992年6月</th>
							<td>資本金を4,800万円に増資</td>
						</tr>
						<tr>
							<th>1993年4月</th>
							<td>北陸事業部・福井工場を操業開始</td>
						</tr>
						<tr>
							<th>1994年5月</th>
							<td>ボックスアンドペーパー株式会社設立</td>
						</tr>
						<tr>
							<th>1997年9月</th>
							<td>北陸事業部・紙器彩感において最新鋭全自動3ライン新設操業開始</td>
						</tr>
						<tr>
							<th>1999年12月</th>
							<td>北陸事業部・福井工場においてISO9001取得</td>
						</tr>
						<tr>
							<th>2000年9月</th>
							<td>北陸事業部・福井工場においてISO14001取得</td>
						</tr>
						<tr>
							<th>2001年9月</th>
							<td>関西事業部・城陽工場において最新鋭全自動1ライン新設稼動</td>
						</tr>
						<tr>
							<th>2005年1月</th>
							<td>関西事業部・城陽工場においてプラテン打抜き機稼動</td>
						</tr>
						<tr>
							<th>2005年8月</th>
							<td>ISO9001/14001の統合取得に変更(JQA)</td>
						</tr>
						<tr>
							<th>2005年9月</th>
							<td>中国事業部・岡山工場において最新鋭全自動1ライン新設操業開始</td>
						</tr>
						<tr>
							<th>2006年3月</th>
							<td>私募債・第1回無担保社債、発行額 金1億円を財務代理人、商工組合中央金庫より決定</td>
						</tr>
						<tr>
							<th>2006年8月</th>
							<td>滋賀県米原市顔戸に敷地面積8580㎡、延床面積6150㎡、物流設備一式の物流センターを操業開始</td>
						</tr>
						<tr>
							<th>2009年4月</th>
							<td>資本金を8,200万円に増資</td>
						</tr>
						<tr>
							<th>2009年5月</th>
							<td>代表取締役社長 松田嶸一へ「京都府紙器段ボール箱工業組合」の理事及び理事長としての長年の活動に対し「藍綬褒章」が授与されました</td>
						</tr>
						<tr>
							<th>2011年4月</th>
							<td>東海事業部・三重工場を操業開始</td>
						</tr>
						<tr>
							<th>2012年9月</th>
							<td>滋賀物流サービス内に段ボール製品製造用のフルライン稼働</td>
						</tr>
					</tbody>
				</table>
			</div>		
	</div>					
</div><!-- end primary-row -->
<?php get_template_part('footer'); ?>