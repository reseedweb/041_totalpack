<?php get_header(); ?>
    <div class="primary-row clearfix"><!-- begin primary-row  -->                    		
			<div class="row clearfix">
				<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
					<h3 class="h3-title">品質・環境理念</h3>
				</div>
			</div>
		   <div class="row clearfix">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 quality-content1-img">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/quality_content_img1.jpg" alt="quality" /></p>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 quality-content1-text">
					<p>顧客満足を再優先事項と位置づけ、事業活動を通し環境影響への低減と新たな明日への創造を確実にするため、 目標を定め、定期的に見直し、継続的改善に努めます。</p>
				</div>
		   </div>
		
		
		<div class="primary-row clearfix">
			<div class="row clearfix">
				<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
					<h3 class="h3-title">品質環境方針</h3>
					<div class="quality-content2">
						<p>1.事業活動のあらゆる業務において、省資源、省エネルギー、リサイクル、廃棄物の低減、汚染の予防に努めます。</p>
						<p>2.目標及び目的を設定し、取り組み結果を見直し、品質・環境マネジメントシステムの維持と有効性の継続的改善に努めます。</p>
						<p>3.環境に関わる法規、規制など遵守し、自主活動による環境の維持向上に積極的に努めます。</p>
						<p>4.お客様に満足していただける製品の提供を前提に、全社員の技術向上と品質の維持に努めます。</p>
						<p>5.地域や社会における環境保護活動に積極的に参画します。</p>
					</div>
				</div>
			</div>
		</div>
		
		<div class="primary-row clearfix">
			<div class="row clearfix">
				<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
					<h3 class="h3-title">国際標準規格取得</h3>
				</div>
			</div>
		   <div class="row mb40 clearfix">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 quality-content3-img">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/quality_content_img2.jpg" alt="quality" /></p>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 quality-content3-text">
					<p>株式会社昭和商会北陸事業部福井工場は、業界に先駆け国際標準規格ISO9001の認証を取得しています。</p>
					<p class="quality-content3-text-space">
						■1999年12月に国際標準規格ISO9001の認証を取得。<br />
						■2000年9月に国際標準規格ISO14001の認証を取得。<br />
						■2005年10月に品質及び環境における最新版の統合取得。
					</p>
				</div>
		   </div>
		</div>
	</div><!-- end primary-row --> 	                                                    		                                                          	                                                                                                                                         	                          
<?php get_footer(); ?>
				