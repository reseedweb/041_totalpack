<?php get_header(); ?>
    <div class="primary-row clearfix"><!-- begin primary-row -->                    
        <div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
                <div class="workshop-main1">										
					<h3 class="workshop-main1-title">三重工場</h3>									
				</div>
            </div>			          
        </div>        
	</div><!-- end primary-row --> 	
   
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-18">
				<h4 class="workshop-title1">街を歩けば</h4>
				<div class="workshop-text1">
					<p>東は伊勢湾に面した港町から西は奈良県境まで、広大な面積を持つ津市は、伊勢平野の中心に位置し、 美しい山と海の自然に恵まれ、四季の変化を感じ、一年を通じて穏やかな気候で生活しやすい環境にある都市です。<br />
					東海事業部は中勢北部サイエンスシティを中心とするオフィスアルカディア内で操業しています。</p>
				</div>
				<h5 class="workshop-title2">所在地</h5>
				<div class="workshop-text2">
					<p>〒514-0131<br />三重県津市あのつ台4-2-4<br />TEL.059-253-6354　FAX.059-231-6238</p>
				</div>
				
				<h5 class="workshop-title2">敷地</h5>
				<div class="workshop-text2">
					<p>敷地面積：6322㎡<br />延床面積：A棟638㎡　B棟660㎡</p>
				</div>
				
				<h5 class="workshop-title2">製造業務内容</h5>
				<div class="workshop-text2">
					<p>大型インクジェット印刷機(5色)<br />段ボール専用インクジェット機(4色)<br />巻取り専用インクジェット機(4色)<br />大型サンプルカッター機<br />プラダンケース・プラダン製品製造<br />段ボールケース・段ボール製品製造</p>
				</div>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-18">
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/mie_content_top.jpg" alt="okayama" /></p>
			</div>
		</div> 		
	</div><!-- end primary-row --> 
	
	<div class="primary-row clearfix"><!-- begin primary-row -->                    
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h3 class="workshop-slider-title">三重工場の写真紹介</h3>
				<?php get_template_part('part','factory');?>
			</div>						
		</div> 
	</div><!-- end primary-row --> 
	<div class="workshop-map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6552.888216834275!2d136.49867303203854!3d34.79476714207578!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60040ac0124e17ab%3A0x353f74d288528dfc!2s4+Chome-2-4+Anotsudai%2C+Tsu-shi%2C+Mie-ken+514-0131%2C+Nh%E1%BA%ADt+B%E1%BA%A3n!5e0!3m2!1svi!2s!4v1432691916349" width="100%" height="470" frameborder="0" style="border:0"></iframe>
	</div>	                                                          		                                                          	                                                                                                                                         	                          
<?php get_footer(); ?>